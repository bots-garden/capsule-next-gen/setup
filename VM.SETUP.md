# VM Setup (🚧 WIP)

I use to develop with [Coder Server](https://code-server.dev) running into [Multipass](https://multipass.run/). If you are interested by this solution, have a look to this project: https://gitlab.com/bots-garden/extism-ide

👋 **Pay attention**: it's a work in progress, and all the scripts are written for an `ARM` architecture (and I run Multipass on a MacBook Pro M1), so you have to modify this script if you are not on `ARM`: https://gitlab.com/bots-garden/extism-ide/-/blob/main/generate-vm.sh

## Once the VM running:  Development setup

Go to https://extism-web-ide.local:9090/?folder=/home/ubuntu/workspaces (your domain could be different, it depends of your configuration), then open a VSCode terminal:


Create a directory `capsule-next-gen` and `git clone` the `capsule*` projects:

```bash
mkdir capsule-next-gen
cd capsule-next-gen
git clone git@gitlab.com:bots-garden/capsule-next-gen/capsule.git
git clone git@gitlab.com:bots-garden/capsule-next-gen/capsule-host-sdk.git
git clone git@gitlab.com:bots-garden/capsule-next-gen/capsule-module-sdk.git
```


```bash
cd capsule-next-gen
touch go.work
```

> content of go.work
```
go 1.20

use (
    ./capsule-host-sdk
    ./capsule-module-sdk
    ./capsule/capsule-cli
    ./capsule/capsule-http
)
```

## VSCode support

```bash
cd capsule-next-gen
mkdir .vscode
cd vscode
touch settings.json
touch extensions.json

```

> Content of `settings.json`

```json
{
  "workbench.iconTheme": "material-icon-theme",
  "workbench.colorTheme": "GitHub Dark Dimmed",
  "terminal.integrated.fontSize": 16,
  "editor.fontSize": 16,
}
```

> Content of `extensions.json`

```json
{
  "recommendations": [
    "pkief.material-icon-theme",
    "pkief.material-product-icons",
    "aaron-bond.better-comments"
  ]
}
```

