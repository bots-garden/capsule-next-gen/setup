# Setup

> If you want to develop with a virtual machine, read `VM.SETUP.md`

## Environment setup

> - Linux version
> - adapt the scripts regarding your architecture

### Install Go and TinyGo

```bash
GOLANG_VERSION="1.20"
GOLANG_OS="linux"
GOLANG_ARCH="arm64"
TINYGO_VERSION="0.28.1"
TINYGO_ARCH="arm64"

# -----------------------
# Install GoLang
# -----------------------

echo "Installing Go & TinyGo"

wget https://go.dev/dl/go${GOLANG_VERSION}.${GOLANG_OS}-${GOLANG_ARCH}.tar.gz

sudo rm -rf /usr/local/go 
sudo tar -C /usr/local -xzf go${GOLANG_VERSION}.${GOLANG_OS}-${GOLANG_ARCH}.tar.gz

echo "" >> ~/.bashrc
echo 'export GOLANG_HOME="/usr/local/go"' >> ~/.bashrc
echo 'export PATH="\$GOLANG_HOME/bin:\$PATH"'  >> ~/.bashrc
source ~/.bashrc

rm go${GOLANG_VERSION}.${GOLANG_OS}-${GOLANG_ARCH}.tar.gz               

# -----------------------
# Install TinyGo
# -----------------------
wget https://github.com/tinygo-org/tinygo/releases/download/v${TINYGO_VERSION}/tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb
sudo dpkg -i tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb
rm tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb

export GOLANG_HOME="/usr/local/go"
export PATH="$GOLANG_HOME/bin:$PATH"

go version
go install -v golang.org/x/tools/gopls@latest
go install -v github.com/ramya-rao-a/go-outline@latest
go install -v github.com/stamblerre/gocode@v1.0.0
```

### Install Node.js

```bash
# -----------------------
# Install NodeJS
# -----------------------

curl -sL https://deb.nodesource.com/setup_19.x | sudo -E bash -
sudo apt-get install -y nodejs
```

### Install Rust

```bash
# -----------------------
# Install Rust support
# -----------------------
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh -s -- -y

echo 'export CARGO_HOME="~/.cargo"' >> ~/.bashrc
echo 'export PATH=\$CARGO_HOME/bin:\$PATH' >> ~/.bashrc

source ~/.cargo/env
source ~/.bashrc

# Add wasm & wasi targets
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
rustup target add wasm32-wasi
rustup target add wasm32-unknown-unknown

# -----------------------
# Install Wasm Bindgen
# -----------------------
cargo install -f wasm-bindgen-cli

rustup component add rust-analysis --toolchain stable-aarch64-unknown-linux-gnu 
rustup component add rust-src --toolchain stable-aarch64-unknown-linux-gnu 
rustup component add rls --toolchain stable-aarch64-unknown-linux-gnu
```

### Install Wasi Runtimes (optional)

```bash

# -----------------------
# Install Wasi Runtimes
# -----------------------

curl -sSf https://raw.githubusercontent.com/WasmEdge/WasmEdge/master/utils/install.sh | bash

source ${HOME}/.wasmedge/env

curl https://get.wasmer.io -sSfL | sh
source ${HOME}/.wasmer/wasmer.sh

curl https://wasmtime.dev/install.sh -sSf | bash
source ${HOME}/.bashrc
```

### Install Extism

```bash
# -----------------------
# Install Extism
# -----------------------
sudo apt-get update -y
sudo apt-get install -y pkg-config

sudo apt install python3-pip -y
pip3 install poetry
pip3 install git+https://github.com/extism/cli

echo "export EXTISM_HOME=\"\$HOME/.local\"" >> ${HOME}/.bashrc
echo "export PATH=\"\$EXTISM_HOME/bin:\$PATH\"" >> ${HOME}/.bashrc

source ${HOME}/.bashrc

extism --prefix=/usr/local install latest
pip3 install extism

# -----------------------
# Install Extism JS PDK
# -----------------------

export TAG="v0.5.0"
export ARCH="aarch64"
export  OS="linux"
curl -L -O "https://github.com/extism/js-pdk/releases/download/$TAG/extism-js-$ARCH-$OS-$TAG.gz"
gunzip extism-js*.gz
sudo mv extism-js-* /usr/local/bin/extism-js
chmod +x /usr/local/bin/extism-js
```

### Install Hey

> Hey is used for load testing with `capsule-http`

```bash
# Install Hey
sudo apt-get update
sudo apt-get -y install hey
```

### Install Taskfile

> - Taskfile is used for various integration tests
> - https://taskfile.dev/

```bash
# 🚧 WIP
```

### Additional tools (optional)

```bash
# ------------------------------
# Install Tools
# ------------------------------
 
# Install Bat
wget https://github.com/sharkdp/bat/releases/download/v${BAT_VERSION}/bat_${BAT_VERSION}_${BAT_ARCH}.deb
sudo dpkg -i bat_${BAT_VERSION}_${BAT_ARCH}.deb
rm bat_${BAT_VERSION}_${BAT_ARCH}.deb      

# Install Exa
sudo apt-get install exa -y

# Install httpie
sudo snap install httpie

# Install OhMyBash
bash -c "\$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"

```

## Development setup

Create a directory `capsule-next-gen` and `git clone` the `capsule*` projects:

```bash
mkdir capsule-next-gen
cd capsule-next-gen
git clone git@gitlab.com:bots-garden/capsule-next-gen/capsule.git
git clone git@gitlab.com:bots-garden/capsule-next-gen/capsule-host-sdk.git
git clone git@gitlab.com:bots-garden/capsule-next-gen/capsule-module-sdk.git
```


```bash
cd capsule-next-gen
touch go.work
```

> content of go.work
```
go 1.20

use (
    ./capsule-host-sdk
    ./capsule-module-sdk
    ./capsule/capsule-cli
    ./capsule/capsule-http
)
```


### VSCode support

```bash
cd capsule-next-gen
mkdir .vscode
cd vscode
touch settings.json
touch extensions.json

```

> Content of `settings.json`

```json
{
  "workbench.iconTheme": "material-icon-theme",
  "workbench.colorTheme": "GitHub Dark Dimmed",
  "terminal.integrated.fontSize": 16,
  "editor.fontSize": 16,
}
```

> Content of `extensions.json`

```json
{
  "recommendations": [
    "pkief.material-icon-theme",
    "pkief.material-product-icons",
    "aaron-bond.better-comments"
  ]
}
```

And you will need this VSCode extensions:

- golang.go
- rust-lang.rust-analyzer
